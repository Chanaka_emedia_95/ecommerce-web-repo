import React from 'react'
import Home from "../Home/Home"
import "bootstrap/dist/css/bootstrap.min.css";
import "./Layout.css"

export default function Layout() {
    return (
        <div className="container-fluid">
 <nav className="main-navigation">
        <div className="navbar-header animated fadeInUp">
            <a className="navbar-brand" href="#">Logo Area</a>
        </div>
        <ul className="nav-list">
            
            <li className="nav-list-item">
                <a href="#" className="nav-link">Login</a>
            </li>
            <li className="nav-list-item">
                <a href="#" className="nav-link">Sign up</a>
            </li>
        </ul>

    </nav>
      <div>
         
      </div>
      <Home/>
    <footer className="footer row mt-5">
	<div className="container mt-5">
		<div className="row ">
			<div className="col-md-12 mt-5 text-center">
				<h1>PURE CSS RGB FOOTER</h1>
				<h4>Enter you text here!!</h4>
				<h4>TEXT</h4>
				<p className="social-icons-header">
					 		<a href="facebook.com"><i className="fa fa-facebook "></i></a>
					 		<a href="facebook.com"><i className="fa fa-twitter"></i></a>
					 		<a href="facebook.com"><i className="fa fa-instagram"></i></a>
					 		<a href="facebook.com"><i className="fa fa-linkedin"></i></a>	
					 </p>
			</div>
			<div className="col-md-12 text-center mt-5">
				<h6 className="">Designed with <span style={{color: "red"}}>❤</span> by <a href="http://www.prat.ee/k">Prateek Gupta</a> </h6>
			</div>
		</div>
	</div>
</footer>
        </div>
    )
}
