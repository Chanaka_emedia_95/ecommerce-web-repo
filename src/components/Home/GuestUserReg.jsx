return (
    <>
      <div className="row">
        <div className="col-md-2">
          {data && data.length?<Categories dataSet={data}/>:""}
        </div>
        <div className="col-md-10"  ref={targetRef}>
          <SimpleImageSlider
            width={dimensions.width-20}
            height={504}
            images={images}
            showBullets={true}
            showNavs={true}
          />
        </div>
      </div>
      <div className="container mydiv">
        <div className="row">
          <ProductCard key="1" />
          <ProductCard key="2" />
          <ProductCard key="3" />
        </div>
      </div>
    </>
  );
export default Home;