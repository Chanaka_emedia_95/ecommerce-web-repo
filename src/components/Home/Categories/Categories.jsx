import React, { useState } from "react";
import { TreeFill } from 'react-bootstrap-icons';
import "./categories.css"

const Categories = (props) => {
  const [jData] = useState(props.dataSet);

  return (
    <div className="category-wrapper">
        <h3 className="text-center">Category</h3>
      <ul className="category-main">
        {jData.map((cate, i) => {
         
          return <li key={i} className="category-item"><TreeFill /> {cate.Name}</li>;
        })}
      </ul>
    </div>
  );
};
export default Categories;
