import React, { useState,useLayoutEffect,useRef } from "react";
import SimpleImageSlider from "react-simple-image-slider";
import ProductCard from "./ProductCard";
import Categories from "./Categories/Categories"
import data from "../../Data/Category/CategoryData.json"

const images = [
  {
    url: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSOo129hKGPPjR8hKHaNMrEAK7KQ-_Uojqfhg&usqp=CAU",
  },
  {
    url: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSOo129hKGPPjR8hKHaNMrEAK7KQ-_Uojqfhg&usqp=CAU",
  },
  {
    url: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSOo129hKGPPjR8hKHaNMrEAK7KQ-_Uojqfhg&usqp=CAU",
  },
];

const Home = () => {
  const targetRef = useRef();
  const [dimensions, setDimensions] = useState({ width:0, height: 0 });

  useLayoutEffect(() => {
    if (targetRef.current) {
      setDimensions({
        width: targetRef.current.offsetWidth,
        height: targetRef.current.offsetHeight
      });
    }
  }, []);

  let intViewportWidth = window.innerWidth;

  return (
    <>
      <div className="row">
        <div className="col-md-2">
          {data && data.length?<Categories dataSet={data}/>:""}
        </div>
        <div className="col-md-10"  ref={targetRef}>
          <SimpleImageSlider
            width={dimensions.width-20}
            height={504}
            images={images}
            showBullets={true}
            showNavs={true}
          />
        </div>
      </div>
      <div className="container mydiv">
        <div className="row">
          <ProductCard key="1" />
          <ProductCard key="2" />
          <ProductCard key="3" />
        </div>
      </div>
    </>
  );
};
export default Home;
